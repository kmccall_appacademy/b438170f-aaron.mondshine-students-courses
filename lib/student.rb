class Student
  attr_accessor :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    first_last = [@first_name, @last_name]
    first_last.join(" ")
  end

  def enroll(new_course)
    courses.each { |course| raise if new_course.conflicts_with?(course) }

    @courses << new_course unless @courses.include?(new_course)
    new_course.students << self
  end

  def course_load
    department_credit = Hash.new(0)
    courses.each do |course|
      department_credit[course.department] += course.credits
    end
    department_credit
  end

end
